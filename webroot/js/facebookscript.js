// This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
      
   // console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      // document.getElementById('status1').innerHTML = JSON.stringify(response);
      testAPI(response);
      
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      //document.getElementById('status').innerHTML = 'Please log ' +
       // 'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      //document.getElementById('status').innerHTML = 'Please log ' +
       // 'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '638729852817054',// techpnat//1629026207354411 //local 960473684003616
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    //statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI(oauth) {
    //console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      //alert(JSON.stringify(response));
     // $('#txtEcho1').html(JSON.stringify(response));
      var pathname=window.location.pathname;
      var pathArray = window.location.pathname.split( '/' );
      var finalurl='';
      if(pathArray[2]=='users')
        finalurl='facebookaction';
      else
        finalurl='users/facebookaction';
      $.ajax({
          url: finalurl,
          type: 'POST',
          data: {'me':JSON.stringify(response),'oauth':JSON.stringify(oauth)},
          success: function(data)
          {
            //$('#status').html(data);
            //$('#status1').html(data);
            //alert(data);
            if(data=='success')
            {
                //alert(window.location.pathname);
                if(pathArray[2]=='users')
                    window.location.href=window.location.href+'/dashboard';
                else
                    window.location.href=window.location.href+'users/dashboard';
            }
            else
            {
                alert(data);
            }
          },
        });
      

      // console.log('Successful login for: ' + response.name);
      // document.getElementById('status').innerHTML =
      //   'Thanks for logging in, '+ JSON.stringify(response) + response.name + '!';
    },{scope: 'public_profile,email'});  
}
function login(){
   FB.login(function(response) {
      if (response.authResponse) {
         // proceed
        testAPI(response);
      } else {
         // not auth / cancelled the login!
      }
   },{scope: 'public_profile,email'});
}

function getPostStatus(){

      $.ajax({
          url: "getPostStatus",
          type: 'POST',
          data: {},
          success: function(data)
          {
            console.log(data);
            
          },
        });
  
}

function getFriends(id){
  var friends=null;
  FB.api('/me/friends', function(response) {
      friends= response.summary.total_count;
      $.ajax({
          url: "facebookFriends",
          type: 'POST',
          data: {'friends':friends,'id':id},
          success: function(data)
          {
            $('#fb_friends').html(friends);
            //$('#status1').html(data);
            
          },
        });

    });
  //return friends;
}

function getProfilePic(id){
  var pic=null;
  FB.api('/me/picture?type=normal', function(response) {
    pic=response.data.url;
    $.ajax({
          url: "facebookProfileImage",
          type: 'POST',
          data: {'url':pic,'id':id},
          success: function(data)
          {
            var str="<img src='"+response.data.url+"' alt='facebook profile picture' class='img-circle pull-right'/>";
             $('#fbProPic').html(str);
            //$('#status1').html(data);
            
          },
        });
          // var str="<br/><b>Pic</b> : <img src='"+response.data.url+"'/>";
          // document.getElementById("status").innerHTML+=str;
    });
  //return pic;
}

function postAdd(id)
{
  var flag=dailyPostChecker();
  if(flag=='false'){
    toastr.error("Your daily limit has been exhausted");
     return;
   }
    // var data=
    // {

    //          message : 'Ateam4adream',
    //          name : 'Ateam4adream virtual project management',
    //          //link : 'http://techphant.com',
    //          privacy: '{value: EVERYONE}',
    //          description : 'Sugerblock description #medicine #e-commerce (endorsed)',
    //          picture : 'http://techphant.com/img/portfolio/SugarBlock4.jpg',

    //  }
    // FB.api('/me/feed', 'post', data, onPostToWallCompleted);
    var imgurl= $('.capimg'+id).attr('src');
    var title= $('.captitle'+id).html();
    var desc= $('.capdesc'+id).html();
    var tag= $('.captag'+id).html();
    FB.ui({
            method: 'feed',
            name : title,
            link : 'http://localhost:12/vod/',
            privacy: {value:"EVERYONE"},
            description : desc + '' + tag,
            picture : imgurl,
}, function(response){  
      location.reload();
      
      $.ajax({
          url: "facebookPost",
          type: 'POST',
          data: {'post_id':response.post_id},
          success: function(data)
          {
            //alert("Successful post on facebook");
            toastr.success("successfully posted on your wall");
          },
        });
   });
}
function postvideoV(id)
{
  var flag=dailyPostChecker();
  if(flag=='false'){
    toastr.error("Your daily limit has been exhausted");
     return;
  }
  var vurl= $('.capvurl'+id).attr('vurl');
  FB.ui({
     method: 'feed',
            //vimeo
            picture: 'https://i.vimeocdn.com/video/'+vurl+'_1280x800.jpg',
            link: 'https://player.vimeo.com/video/'+vurl+'?autoplay=1',
            source:'https://vimeo.com/moogaloop.swf?clip_id='+vurl+'&amp;autoplay=1',    
            type: 'video',
}, function(response){  
      
      $.ajax({
          url: "facebookPost",
          type: 'POST',
          data: {'post_id':response.post_id},
          success: function(data)
          {
             //alert(data);
            //$('#status1').html(data);
            toastr.success("successfully posted on your wall");
            location.reload();
            
          },
        });
   });
}
function postvideoY(id)
{
   
  var flag=dailyPostChecker();
  if(flag=='false'){
    toastr.error("Your daily limit has been exhausted");
     return;
  }

  var yurl= $('.capyurl'+id).attr('yurl');
  FB.ui({
     method: 'feed',
            
            //you tube
            picture: 'http://i.ytimg.com/vi/'+yurl+'/maxresdefault.jpg',
            link: ' https://www.youtube.com/embed/'+yurl+'',
            source:'https://www.youtube.com/v/'+yurl+'?autohide=1&amp;version=3',    
            type: 'video',
            //name: 'Google',
            // description: 'Test video',
            //source: 'https://vimeo.com/moogaloop.swf?clip_id=131737680&amp;autoplay=1',
}, function(response){  
      //alert(response.post_id);
      location.reload();
      $.ajax({
          url: "facebookPost",
          type: 'POST',
          data: {'post_id':response.post_id},
          success: function(data)
          {
             //alert(data);
            //$('#status1').html(data);
            toastr.success("successfully posted on your wall");
            
          },
        });
   });
}
function updateProfilePic(){

  FB.api("/me/photos", 'post', {
   message: 'Update profle picture',
   status: 'success',
   url: 'http://www.20blinks.com/usercontent/7/d/4293/avatar1416491759.jpg' ,
   }, function (response) {
      if (!response || response.error) {
       $('#status1').html(JSON.stringify(response));
       } else {
        $('#status1').html(JSON.stringify(response));
         getAlbum(response.id);
       }
   });    
}
function getAlbum(id){

    FB.api('/'+id, function(response) {
      $('#status1').html(JSON.stringify(response));
      //top.location.href = response.link+'&makeprofile=1';
      top.location.href = 'https://www.facebook.com/profile.php?preview_cover='+id;
    });
    //cover photo url //https://www.facebook.com/profile.php?preview_cover=123455681324064
    //preview_cover=recent_uploaded_image_id
}

function data(id){
var vurl= $('.capvurl'+id).attr('vurl');
// var title= $('.captitle'+id).html();
// var desc= $('.capdesc'+id).html();
// var tag= $('.captag'+id).html();
alert(vurl);
}

function dailyPostChecker()
{
   var flag = 'true'; 
   $.ajax({
          url: "facebookDailyLimit",
          type: 'POST',
          success: function(data)
          {
            
            flag=data;
          },
           async: false, 
        });
   return flag;
}

// 

$(document).ready(function(){

    //  $(".fb").click(function(){
    // newWin = window.open('https://facebook.com',
    //             "_blank",
    //             "toolbar=no, location=yes, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=yes, width=500, height=400");
    // });

     $(".in").click(function(){
     var add =$(this).attr('add');
   
    newWin = window.open('postlinkedin?uri='+add+'&eraseCache=true',   //window.open('postlinkedin/'+add+'',
                "_blank",
                "toolbar=no, location=yes, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=yes, width=500, height=400");
     // newWin.beforeunload = refreshParent;
     
    });
     $(".tw").click(function(){
       var add =$(this).attr('add');
       var post =$(this).attr('post');
       //window.location.origin -for host name
       window.location.href= 'retweet/'+add+'/'+post+'?eraseCache=true&uri=';
        return false;
       // window.location.href= '<?php echo SITEURL;?>/Users/loinTwitter/tweet?eraseCache=true&uri='+add+'';
       //  return false;
    /*newWin = window.open('loinTwitter/tweet',
                "_blank",
                "toolbar=no, location=yes, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, copyhistory=yes, width=500, height=400");*/
    //window.onload = window.reload();

    });


});

