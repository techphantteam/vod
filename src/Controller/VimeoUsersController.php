<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * VimeoUsers Controller
 *
 * @property \App\Model\Table\VimeoUsersTable $VimeoUsers
 */
class VimeoUsersController extends AppController {

    /**
     * Index method
     *
     * @return void
     */
    public function vimeoindex() {

        //$this->VimeoUsers->find('all',['order'=>'id']);
        $this->request->data['client_id'] = $this->Api->getClientId();
        $this->request->data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $this->request->data);
        $response_api = json_decode($this->Curl->callCurl($this->Api->getUrl() . "/VimeoUsers/index.json", $this->request->data));
        if ($response_api->message == 'Success' && $response_api->code == '200') {
            $this->set('vimeouser', (array) $response_api->msgstatus);
            // $this->redirect(['controller' => 'vimeo_users', 'action' => 'index']);
        } else if ($response_api->message == 'Error' && $response_api->code == '406') {
            $this->Flash->error('No information found', 'vimeoapi1');
            // $this->redirect(['controller' => 'vimeo_users', 'action' => 'index']);
        } else if ($response_api->message == 'Invalid operation' && $response_api->code == '404') {
            $this->Flash->error('Your API is not valid, please apply valid api key', 'vimeoapi');
            //$this->redirect(['controller' => 'vimeo_users', 'action' => 'index']);
        } else if ($response_api->code == '500') {
            $this->Flash->error($response_api->message, 'vimeoapi');
            //$this->redirect(['controller' => 'vimeo_users', 'action' => 'index']);
        }
        $this->set('vimeoUsers', (array) $response_api);
        $this->set('_serialize', ['vimeoUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Vimeo User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function vimeoview($id = null) {
         if ($id == "") {
            $this->Flash->error(__('Your Url is invalid, try again.'));
            return $this->redirect(['controller' => 'vimeo_users', 'action' => 'vimeoindex']);
        }
        $vimeoUser = $this->VimeoUsers->get($id, [
            'contain' => []
        ]);
        $this->set('vimeoUser', $vimeoUser);
        $this->set('_serialize', ['vimeoUser']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function vimeoadd() {

        if ($this->request->is('post')) {
            //$this->request->data['userid'] = $this->request->Session()->read('userdata')->id;
            $this->request->data['client_id'] = $this->Api->getClientId();
            $this->request->data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $this->request->data);
            $response_api = $this->Curl->callCurl($this->Api->getUrl() . "/VimeoUsers/add.json", $this->request->data);
            $api_info = json_decode($response_api);
            if ($api_info->message == 'Saved' && $api_info->code == '200') {
                $this->Flash->success('Vimeo API information has been successfully saved', 'vimeoapi');
                $this->redirect(['controller' => 'vimeo_users', 'action' => 'vimeoindex']);
            } else if ($api_info->message == 'Error' && $api_info->code == '406') {
                $this->request->Session()->write('errorflash', (array) $api_info->errormessage);
                $this->Flash->error('Vimeo API information not successfully saved, Please try again.', 'vimeoapi1');
                $this->redirect(['controller' => 'vimeo_users', 'action' => 'vimeoadd']);
            } else if ($api_info->message == 'Invalid operation' && $api_info->code == '404') {
                $this->Flash->error('Your API is not valid, please apply valid api key', 'vimeoapi');
                $this->redirect(['controller' => 'vimeo_users', 'action' => 'vimeoindex']);
            } else if ($api_info->code == '500') {
                $this->Flash->error($api_info->message, 'vimeoapi');
                $this->redirect(['controller' => 'vimeo_users', 'action' => 'vimeoindex']);
            }
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Vimeo User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function vimeoedit($id = null) {

        if ($id == "") {
            $this->Flash->error(__('Your Url is invalid, try again.'));
            return $this->redirect(['controller' => 'vimeo_users', 'action' => 'vimeoindex']);
        }
        $vimeoUser = $this->VimeoUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $vimeoUser = $this->VimeoUsers->patchEntity($vimeoUser, $this->request->data);
            if ($this->VimeoUsers->save($vimeoUser)) {
                $this->Flash->success(__('The vimeo user API has been successfully edit.'));
                return $this->redirect(['action' => 'vimeoindex']);
            } else {
                $this->Flash->error(__('The vimeo user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('vimeoUser'));
        $this->set('_serialize', ['vimeoUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Vimeo User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function vimeodelete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $vimeoUser = $this->VimeoUsers->get($id);
        if ($this->VimeoUsers->delete($vimeoUser)) {
            $this->Flash->success(__('The vimeo user API has been deleted.'));
        } else {
            $this->Flash->error(__('The vimeo user API could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'vimeoindex']);
    }
    
    public function viewactive(){
        $this->autoRender=false;
        $this->request->data['client_id'] = $this->Api->getClientId();
        $this->request->data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $this->request->data);
        $response_api = $this->Curl->callCurl($this->Api->getUrl() . "/VimeoUsers/viewactive.json", $this->request->data);
        $api_info = json_decode($response_api);
        echo '<pre>';
        print_r($api_info);
    }
    public function activate($id = null){
        $this->autoRender=false;
        if ($id == "") {
            $this->Flash->error(__('Your Url is invalid, try again.'));
            return $this->redirect(['controller' => 'vimeo_users', 'action' => 'vimeoindex']);
        }
        if($this->VimeoUsers->updateAll(['active' => 1], ['id' => $id])){
             $this->Flash->success(__('Vimeo User API has been successfully updated'));
            return $this->redirect(['controller' => 'vimeo_users', 'action' => 'vimeoindex']);
        }else{
             $this->Flash->error(__('Vimeo API not successfully updated.,try again'));
            return $this->redirect(['controller' => 'vimeo_users', 'action' => 'vimeoindex']);
        }

    }

}
