<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

//    public function initialize() {
//        parent::initialize();
//        $this->loadComponent('Curl');
//        $this->loadComponent('RequestHandler');
//        $this->loadComponent('ManualSession');
//        $this->ManualSession->setAllowedAction(array("login", "signup", "logout"));
//        $this->ManualSession->setControllerActoin("users", "admins");
//    }
//    public function beforeFilter(\Cake\Event\Event $event) {
//        parent::beforeFilter($event);
//        $sessiondata = $this->request->Session()->read('userdata');
//        if (($this->request->params['action'] == 'login') || ($this->request->params['action'] == 'signup')) {
//            if (($this->request->Session()->check('userdata')) == 1) {
//                //if (isset($sessiondata) && !empty($sessiondata->id) && ($sessiondata->role == 'Creator')) {
//                if (($this->request->Session()->read('userdata')->role == 'Creator')) {
//                    $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
//                } else if (($this->request->Session()->read('userdata')->role == 'Admin')) {
//                    $this->redirect(['controller' => 'admins', 'action' => 'dashboard']);
//                }
//                // 
//            }
//        }
//
//        if (!$this->ManualSession->checkAllow($this->request->params['action'])) {
//            $returnsessionval = $this->ManualSession->Sessioncheck();
//            if ($returnsessionval[0] != 1) {
//                return $this->redirect(['controller' => 'users', 'action' => 'login']);
//            }
//        }
//    }

    public $helpers = [
        'Form' => [
            'className' => 'Bootstrap3.BootstrapForm',
            'useCustomFileInput' => true
        ]
    ];

    //var $layout="login_layout";

    /**
     * Index method
     *
     * @return void
     */
    public function login() {
        $this->layout = 'login';
        if ($this->request->is('post')) {
            $this->request->data['client_id'] = $this->Api->getClientId();
            $this->request->data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $this->request->data);
            $signin_data=$this->Curl->callCurl($this->Api->getUrl() . "/Users/login.json", $this->request->data);
            $get_user_info = json_decode($signin_data);
            if ($get_user_info->message == 'Success' && $get_user_info->code == '200') {
                $this->ManualSession->Sessionwrite($get_user_info->userdata);
                $returnval = $this->ManualSession->Sessioncheck();
                if ($returnval[0] == 1) {
                    $this->Flash->success('successfully user login.', 'usersignup');
                    $this->redirect(['controller' => 'users', 'action' => 'dashboard']);
                }
            } else if ($get_user_info->message == 'Error' && $get_user_info->code == '406') {
                $this->Flash->error('User No Found Please try again leter.', 'usersignup');
            } else if ($get_user_info->message == 'Invalid operation' && $get_user_info->code == '404') {
                $this->Flash->error('Your API is not valid, please apply valid api key', 'usersignup');
            } else if ($get_user_info->code == '500') {
                $this->Flash->error($get_user_info->message, 'usersignup');
            }
            
        }
    }

    public function fblogin() {
        $this->autoRender = false;
        if ($this->request->is('Ajax')) {
            $this->disableCache();
            echo json_decode($this->request->data);
            //$data = $this->RequestHandler->renderAs($this->request->data['userdata'], 'json');
            // print_r($data);
            die;
        }
    }

    public function signup() {
        $this->layout = 'login';
        if ($this->request->is('post')) {
          $signupdata = $this->request->data;
          $this->request->data['client_id'] = $this->Api->getClientId();
          $this->request->data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $this->request->data);
          $data=$this->Curl->callCurl($this->Api->getUrl() . "/Users/signup.json", $this->request->data);
          $response_api =json_decode($data); 
          if ($response_api->message == 'Success' && $response_api->code == '200') { 
               $this->Flash->success('You are successfully register');
               $this->redirect(['controller' => 'Users', 'action' => 'signup']);
               } else if ($response_api->message == 'Error' && $response_api->code == '406') {
               $this->Flash->error('You are not successfully registered');
               $this->set('response_api',$response_api);
               //$this->redirect(['controller' => 'Users', 'action' => 'signup']);
            } else if ($response_api->message == 'Invalid operation' && $response_api->code == '404') {
                $this->Flash->error('Your API is not valid, please apply valid api key');
                $this->redirect(['controller' => 'Users', 'action' => 'signup']);
            } else if ($response_api->code == '500') {
                $this->Flash->error($response_api->message);
                $this->redirect(['controller' => 'Users', 'action' => 'signup']);
            }
        }
    }
    public function index() {
        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editprofile($id = null) {
//        $user = $this->Users->get($id, [
//            'contain' => []
//        ]);
  
        $user=$this->request->session()->read('userdata');
        //print_r($user);exit();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function demo() {
        $this->layout = 'admin';
        print_r($returnval = $this->request->Session()->read('userdata'));
    }

    public function dashboard() {
        $layout = $this->ManualSession->setLayout();
        // $this->layout =$layout;
      
    }

    public function logout() {

        $this->request->session()->delete('userdata');
        $this->redirect(['controller' => 'users', 'action' => 'login']);
    }

    public function videoapproval() {
        
    }
    public function settings() {
   
    }
    public function payments() {
        
    }
    public function facebookaction()
    {
        $this->autoRender=false;
        if ($this->request->is('ajax')) {
            $me= json_decode($this->request->data['me']);
            $this->request->data['firstname']=$me->first_name;
            $this->request->data['lastname']=$me->last_name;
            $this->request->data['email']=$me->email;
            $this->request->data['gender']=$me->gender;
            $this->request->data['facebookuser']=$me->id;
            $this->request->data['username']=$me->email;
            $this->request->data['countries_id']=2;
            $this->request->data['cities_id']=2;
            $this->request->data['role']='Creator';
            unset($this->request->data['me']);
            $signupdata = $this->request->data;
            $this->request->data['client_id'] = $this->Api->getClientId();
            $this->request->data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $this->request->data);
            $data=$this->Curl->callCurl($this->Api->getUrl() . "/Users/signupfacebook.json", $this->request->data);
            $response_api =json_decode($data); 
            if ($response_api->message == 'Success' && $response_api->code == '200') {
                $this->ManualSession->Sessionwrite($response_api->userdata);
                $returnval = $this->ManualSession->Sessioncheck();
                if ($returnval[0] == 1) {
                    $this->Flash->success('successfully user login.', 'usersignup');
                    echo "success";
                }
            } else if ($response_api->message == 'Invalid operation' && $response_api->code == '404') {
               echo "Your API is not valid, please apply valid api key";
            } else if ($response_api->code == '500') {
                echo $response_api->message;
            }
        }
    }
}
