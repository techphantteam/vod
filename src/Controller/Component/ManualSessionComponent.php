<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class ManualSessionComponent extends Component {

    protected $allowActions = array();
    protected $approval = array(
        'SuperAdmin' => array('dashboard', 'settings', 'payments','logout','index','vimeoadd'),
        'Admin' => array(
            'dashboard', 'settings', 'uploadvideo',
            'viewvideo', 'videoapproval', 'editvideo', 
            'deletevideo','logout','add',
            'vimeoindex','vimeoadd','vimeoedit','vimeodelete','vimeoview','viewactive','activate',
            'genreindex','genreadd','genreedit','genreview'
            ),
        'Creator' => array('dashboard', 'settings', 'uploadvideo', 'viewvideo','logout','add','edit','uploadAjax','index','ajaxData','view','postcomment'),
    );

    public function Sessionwrite($get_user_info) {
        $this->request->Session()->write('userdata', $get_user_info);
        $this->Sessioncheck();
    }

    public function Sessioncheck() {
        $sessionval = array();
        if (($this->request->Session()->check('userdata')) == 1) {
            if (($this->request->Session()->read('userdata')->role == 'Creator')) {
                $sessionval = array(1, 'Creator');
            } else if (($this->request->Session()->read('userdata')->role == 'Admin')) {
                $sessionval = array(1, 'Admin');
            }else if (($this->request->Session()->read('userdata')->role == 'Super Admin')) {
                $sessionval = array(1, 'Super Admin');
            }
            return $sessionval;
            // Session Exists
        } else {
            return 0; // No Exists
        }
    }

    public function setAllowedAction($actions) {
        $this->allowActions = $actions;
    }

    public function checkAllow($action) {
        if (in_array($action, $this->allowActions)) {
            return true;
        } else {
            return false;
        }
    }

    public function setLayout() {
        if ($this->request->Session()->read('userdata')->role == 'Creator') {
            return 'creator';
        }
        if ($this->request->Session()->read('userdata')->role == 'Admin') {
            return 'admin';
        }
    }

    public function approval($action) {
        if (($this->request->Session()->check('userdata')) == 1) {
            if ($this->request->Session()->read('userdata')->role == 'Creator') {
                if (in_array($action, $this->approval['Creator'])) {
                    return 1;
                } else {
                    return 3;
                }
            }
             else if ($this->request->Session()->read('userdata')->role == 'Admin') {
                if (in_array($action, $this->approval['Admin'])) {
                    return 1;
                } else {
                    return 3;
                }
            }else if ($this->request->Session()->read('userdata')->role == 'Super Admin') {
                if (in_array($action, $this->approval['SuperAdmin'])) {
                    return 1;
                } else {
                    return 3;
                }
            }
        } else {
            return 0;
        }
      
    }
    
}
