<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Component;

use Cake\Controller\Component;

/**
 * CakePHP ApiComponent
 * @author user
 */
class ApiComponent extends Component {
    
    protected $client_id,$secrate_key,$url;
    
    public function __ApiComponent($client_id,$secrate_key,$url){
        $this->client_id=$client_id;
        $this->secrate_key=$secrate_key;
        $this->url=$url;
    }
    
    public function setClientId($id){
        $this->client_id=$id;
    }
    
    public function getClientId(){
        return $this->client_id;
    }
    
    public function setSecrateKey($id){
        $this->secrate_key=$id;
    }
    
    public function getSecrateKey(){
        return $this->secrate_key;
    }
    
    public function setUrl($value){
        $this->url=$value;
    }
    
    public function getUrl(){
        return $this->url;
    }
    
    public function getHashValue($algo,$secratekey,$data){
        $data1="";
        foreach($data as $k=>$v){
            if($data1==""){
                $data1=$v;
            }else{
                $data1.="|".$v;
            }
        }
        return hash_hmac($algo, $data1, $secratekey);
    }
}
