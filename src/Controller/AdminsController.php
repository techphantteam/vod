<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class AdminsController extends AppController {
    
    public function dashboard(){
           $this->layout = 'admin';
    }
     public function logout() {
         
        $this->request->session()->delete('userdata');
        $this->request->session()->delete('admincontroller');
        $this->redirect(['controller' => 'users', 'action' => 'login']);
    }
}
?>