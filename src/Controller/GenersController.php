<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Geners Controller
 *
 * @property \App\Model\Table\GenersTable $Geners
 */
class GenersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function genreindex()
    {
        $this->set('geners', $this->paginate($this->Geners));
        $this->set('_serialize', ['geners']);
    }

    /**
     * View method
     *
     * @param string|null $id Gener id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function genreview($id = null)
    {
        $gener = $this->Geners->get($id, [
            'contain' => []
        ]);
        $this->set('gener', $gener);
        $this->set('_serialize', ['gener']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function genreadd()
    {
        $gener = $this->Geners->newEntity();
        if ($this->request->is('post')) {
            $gener = $this->Geners->patchEntity($gener, $this->request->data);
            if ($this->Geners->save($gener)) {
                $this->Flash->success(__('The gener has been saved.'));
                return $this->redirect(['action' => 'genreindex']);
            } else {
                $this->Flash->error(__('The gener could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('gener'));
        $this->set('_serialize', ['gener']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Gener id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function genreedit($id = null)
    {
        $gener = $this->Geners->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gener = $this->Geners->patchEntity($gener, $this->request->data);
            if ($this->Geners->save($gener)) {
                $this->Flash->success(__('The gener has been saved.'));
                return $this->redirect(['action' => 'genreindex']);
            } else {
                $this->Flash->error(__('The gener could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('gener'));
        $this->set('_serialize', ['gener']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gener id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function genredelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gener = $this->Geners->get($id);
        if ($this->Geners->delete($gener)) {
            $this->Flash->success(__('The gener has been deleted.'));
        } else {
            $this->Flash->error(__('The gener could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'genreindex']);
    }
}
