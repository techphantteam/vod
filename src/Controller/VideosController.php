<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing;
use Vimeo\Vimeo;
use Vimeo\Exceptions\VimeoUploadException;
use Vimeo\Exceptions\VimeoRequestException;
use Cake\I18n\Time;
/**
 * Videos Controller
 *
 * @property \App\Model\Table\VideosTable $Videos
 */
class VideosController extends AppController {

    public $helpers = [
        'Form' => [
            'className' => 'Bootstrap3.BootstrapForm',
            'useCustomFileInput' => true
        ],
        'Url'
    ];
    public $components = array('RequestHandler');
    public static $status = 0;

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Api');
        $this->loadComponent('Curl');
        $this->loadComponent('ManualSession');
        //$this->ManualSession->setAllowedAction(array("status"));
        //$this->Api->__ApiComponent("0186e647471f81ab4ceab0f019cad5ea", "24b2e81164f2822bcf9232539792d19408d17768", "http://localhost/webapp");
    }

    public function beforeRender(\Cake\Event\Event $event) {
        parent::beforeRender($event);
        $role = "Admin";
        if ($role == "Admin") {
            //$this->layout = "admin";
        }
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->set('videos', $this->paginate($this->Videos));
        $this->set('_serialize', ['videos']);
    }

    /**
     * View method
     *
     * @param string|null $id Video id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $video = $this->Videos->get($id, [
            'contain' => []
        ]);
        $this->request->data['videos_id']=$id;
        $this->request->data['client_id'] = $this->Api->getClientId();
        $this->request->data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $this->request->data);
        $data=$this->Curl->callCurl($this->Api->getUrl() . "/Comments/commentData.json", $this->request->data);
        //echo $time=Time::createFromFormat('Y-m-d H:i:s','America/New_York');
        $data=  json_decode($data);
        if ($data->response->code != "200") {
              $data='';
        }
        $user=$this->request->session()->read('userdata');
        $this->set('user_details', $this->request->Session()->read('userdata'));
        $this->set('_serialize', ['video']);
        $this->set(compact('video','user','data'));
        
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    
    public function uploadvideo(){
        
    }
    public function add() {
        //$video = $this->Videos->newEntity();
        if ($this->request->is('post')) {
            /* $this->request->data['userid']="1";
              $postdata=$this->request->data;
              $data="";
              foreach ($postdata as $k=>$v){
              if($data==""){
              $data=$v;
              }else{
              $data.="|".$v;
              }
              }
              $hash_value=  hash_hmac('sha1', $data, $this->Api->getSecrateKey());
              $this->request->data['hash_value']=$hash_value;
              $response_data=$this->Curl->callCurl("http://localhost/videoapi/videos/add.json", $this->request->data);
              die; */

            //Unused Code

            /* $curl=new Component\CurlComponent($registry);
              echo $curl->callCurl("http://localhost/videoapi/add", $this->request->data); */
            //echo 
            /* $video = $this->Videos->patchEntity($video, $this->request->data);
              if ($this->Videos->save($video)) {
              $this->Flash->success(__('The video has been saved.'));
              return $this->redirect(['action' => 'index']);
              } else {
              $this->Flash->error(__('The video could not be saved. Please, try again.'));
              } */
        }
        /* $this->set(compact('video'));
          $this->set('_serialize', ['video']); */
    }

    /**
     * Edit method
     *
     * @param string|null $id Video id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        /* $video = $this->Videos->get($id, [
          'contain' => []
          ]);
          echo "<pre>";
          print_r($video);
          die; */
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['userid'] = "1";
            $this->request->data['client_id'] = $this->Api->getClientId();
            $this->request->data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $this->request->data);
            
            $response_data = json_decode($this->Curl->callCurl($this->Api->getUrl() . "/videos/edit/" . $this->request->data['id'] . ".json", $this->request->data));
            
            
            if ($response_data->response->code == "200") {
                $this->Flash->success(__('The video has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
//                echo "<pre>";
//                print_r($response_data);
//                die;
                $this->set('id', $this->request->data['id']);
                $this->Flash->error(__('The video could not be saved. Please, try again and fill all required fields.'));
            }
            //if($response_data->code)
            /* $video = $this->Videos->patchEntity($video, $this->request->data);
              if ($this->Videos->save($video)) {
              $this->Flash->success(__('The video has been saved.'));
              return $this->redirect(['action' => 'index']);
              } else {
              $this->Flash->error(__('The video could not be saved. Please, try again.'));
              } */
        } else {
            $data = array();
            $data['id'] = $id;
            $data['client_id'] = $this->Api->getClientId();
            $data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $data);
            $response_data = $this->Curl->callCurl($this->Api->getUrl() . "/videos/view.json", $data);
            /* echo "<pre>";
              print_r($response_data); */
            $video1 = json_decode($response_data);
            $video = new \App\Model\Entity\Video();
            foreach ((array) $video1->response->data as $k => $v) {
                $video->set($k, $v);
            }
//            echo "<pre>";
//            print_r($video);
//            die;
            $this->set('id', $id);
        }
        $this->set(compact('video'));
        $this->set('_serialize', ['video']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Video id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $video = $this->Videos->get($id);
        if ($this->Videos->delete($video)) {
            $this->Flash->success(__('The video has been deleted.'));
        } else {
            $this->Flash->error(__('The video could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function ajaxData() {
        $this->autoRender = false;

        /*$table = 'videos';
        $primaryKey = 'id';
        $columns = array(
            array('db' => 'id', 'dt' => 0),
            array('db' => 'video_title', 'dt' => 1),
            array('db' => 'video_link', 'dt' => 2),
            array('db' => 'id', 'dt' => 3)
        );

        $sql_details = array(
            'user' => 'root',
            'pass' => '',
            'db' => 'video',
            'host' => 'localhost'
        );

        //require( 'ssp.class.php' );
        
        echo json_encode(
                \SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
        );*/

         $this->loadModel('Videos');
          $output=$this->Videos->GetData();
          echo json_encode($output); 
    }

    public function status() {
        $this->autoRender = false;
        $data = array();
        if (isset($_REQUEST)) {
            //$vc=new VideosController()
            $data['video_link'] = $_REQUEST['video_uri'];
            $data['client_id'] = $this->Api->getClientId();
            $data['userid'] = 1;  //current usrs login id
            $data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $data);
            //return $data;
        } else {
            echo "0";
        }
        echo $response = $this->Curl->callCurl($this->Api->getUrl() . "/videos/add.json", $data);
    }

    public function ajaxUpload() {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            //print_r($_FILES);
            $client_id = "89cb3629b697c328006d4d9fa8a8ea8459ca97b1";
            $client_secrate = "uEg1KmobPPeGgfK3jhh8gsJxcitQM5NkpIaISQy4zmJcrzyfo7k12nGaw08dNaale1CwRRLpI4qNFA+NXL4TtoeZw0j2M440ETaxIj09qMd4+j3hF1aHzjgULn4VX2Zl";
            $access_token = "1b9a444c8bd682460f8c6dc4f60b6fea";

            $vid = new Vimeo($client_id, $client_secrate, $access_token);
            $ret = $vid->upload($_FILES['file']['tmp_name']);
            print_r($ret);
        }
    }

    public function uploadAjax() {
        $this->autoRender = false;
        $client_id = "89cb3629b697c328006d4d9fa8a8ea8459ca97b1";
        $client_secrate = "uEg1KmobPPeGgfK3jhh8gsJxcitQM5NkpIaISQy4zmJcrzyfo7k12nGaw08dNaale1CwRRLpI4qNFA+NXL4TtoeZw0j2M440ETaxIj09qMd4+j3hF1aHzjgULn4VX2Zl";
        $access_token = "1b9a444c8bd682460f8c6dc4f60b6fea";

        $upgrade_to_1080 = false;
        $machine_id = null;
        $vid = new Vimeo($client_id, $client_secrate, $access_token);

        $file_path = $_FILES['file']['tmp_name'];
        // Validate that our file is real.
        if (!is_file($file_path)) {
            throw new VimeoUploadException('Unable to locate file to upload.');
        }

        // Begin the upload request by getting a ticket
        $ticket_args = array('type' => 'streaming', 'redirect_url' => 'http://localhost/videoproj/videos/status/', 'upgrade_to_1080' => $upgrade_to_1080);
        if ($machine_id !== null) {
            $ticket_args['machine_id'] = $machine_id;
        }
        $ticket = $vid->request('/me/videos', $ticket_args, 'POST');

        if ($ticket['status'] != 201) {
            $ticket_error = !empty($ticket['body']['error']) ? '[' . $ticket['body']['error'] . ']' : '';
            throw new VimeoUploadException('Unable to get an upload ticket.' . $ticket_error);
        }

        // We are going to always target the secure upload URL.
        $url = $ticket['body']['upload_link_secure'];

        // We need a handle on the input file since we may have to send segments multiple times.
        $file = fopen($file_path, 'r');

        // PUTs a file in a POST....do for the streaming when we get there.
        $curl_opts = array(
            CURLOPT_PUT => true,
            CURLOPT_INFILE => $file,
            CURLOPT_INFILESIZE => filesize($file_path),
            CURLOPT_UPLOAD => true,
            CURLOPT_HTTPHEADER => array('Expect: ', 'Content-Range: replaced...')
        );

        // These are the options that set up the validate call.
        $curl_opts_check_progress = array(
            CURLOPT_PUT => true,
            CURLOPT_HTTPHEADER => array('Content-Length: 0', 'Content-Range: bytes */*')
        );

        // Perform the upload by streaming as much to the server as possible and ending when we reach the filesize on the server.
        $size = filesize($file_path);
        $server_at = 0;
        do {
            // The last HTTP header we set MUST be the Content-Range, since we need to remove it and replace it with a proper one.
            array_pop($curl_opts[CURLOPT_HTTPHEADER]);
            $curl_opts[CURLOPT_HTTPHEADER][] = 'Content-Range: bytes ' . $server_at . '-' . $size . '/' . $size;

            fseek($file, $server_at);   //  Put the FP at the point where the server is.

            try {
                $vid->request1($url, $curl_opts);   //Send what we can.
            } catch (\Vimeo\Exceptions\VimeoRequestException $exception) {
                // ignored, it's likely a timeout, and we should only consider a failure from the progress check as a legit failure
            }

            $progress_check = $vid->request1($url, $curl_opts_check_progress); //  Check on what the server has.
            // Figure out how much is on the server.
            list(, $server_at) = explode('-', $progress_check['headers']['Range']);
            $server_at = (int) $server_at;
        } while ($server_at < $size);

        // Complete the upload on the server.
        $completion = $vid->request($ticket['body']['complete_uri'], array(), 'DELETE');

        // Validate that we got back 201 Created
        $status = (int) $completion['status'];
        if ($status != 201) {
            $error = !empty($completion['body']['error']) ? '[' . $completion['body']['error'] . ']' : '';
            throw new VimeoUploadException('Error completing the upload.' . $error);
        } else {
            $data = array();
            $data['video_link'] = $completion['headers']['Location'];
            $data['client_id'] = $this->Api->getClientId();
            $data['userid'] = 1;  //current usrs login id
            $data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $data);
            echo $response = $this->Curl->callCurl($this->Api->getUrl() . "/videos/add.json", $data);
        }
    }
    public function postcomment()
    {
        $this->autoRender=false;
        if ($this->request->is('ajax')) {
           $this->request->data['videos_id']=$this->request->data['videos_id'];
           $this->request->data['users_id']=$this->request->data['users_id'];
           $this->request->data['comment_id']=$this->request->data['comment_id'];
           $this->request->data['comments']=$this->request->data['comments'];
           $this->request->data['name']=$this->request->data['name'];
           $this->request->data['client_id'] = $this->Api->getClientId();
           $this->request->data['hash_value'] = $this->Api->getHashValue('sha1', $this->Api->getSecrateKey(), $this->request->data);
           $data=$this->Curl->callCurl($this->Api->getUrl() . "/Comments/add.json", $this->request->data);
           //$response_api =json_decode($data);
           print_r($data);
        }
    }

}
