<?php
namespace App\Model\Table;
use App\Model\Entity\Video;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Routing\Router;
/**
 * Videos Model
 *
 */
class VideosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('videos');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->add('userid', 'valid', ['rule' => 'numeric'])
            ->requirePresence('userid', 'create')
            ->notEmpty('userid');
            
        $validator
            ->requirePresence('genres', 'create')
            ->notEmpty('genres');
            
        $validator
            ->requirePresence('topics', 'create')
            ->notEmpty('topics');
            
        $validator
            ->requirePresence('tags', 'create')
            ->notEmpty('tags');
            
        $validator
            ->add('countryid', 'valid', ['rule' => 'numeric'])
            ->requirePresence('countryid', 'create')
            ->notEmpty('countryid');
            
        $validator
            ->requirePresence('video_title', 'create')
            ->notEmpty('video_title');
            
        $validator
            ->requirePresence('video_year', 'create')
            ->notEmpty('video_year');
            
        $validator
            ->requirePresence('video_description', 'create')
            ->notEmpty('video_description');
            
        $validator
            ->requirePresence('video_director', 'create')
            ->notEmpty('video_director');
            
        $validator
            ->requirePresence('video_synopsis', 'create')
            ->notEmpty('video_synopsis');
            
        $validator
            ->requirePresence('country', 'create')
            ->notEmpty('country');
            
        $validator
            ->requirePresence('film_festival', 'create')
            ->notEmpty('film_festival');
            
        $validator
            ->requirePresence('language', 'create')
            ->notEmpty('language');
            
        $validator
            ->requirePresence('video_logline', 'create')
            ->notEmpty('video_logline');
            
        $validator
            ->requirePresence('video_cast', 'create')
            ->notEmpty('video_cast');
            
        $validator
            ->requirePresence('video_link', 'create')
            ->notEmpty('video_link')
            ->add('video_link', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);
            
        $validator
            ->add('video_length', 'valid', ['rule' => 'numeric'])
            ->requirePresence('video_length', 'create')
            ->notEmpty('video_length');
            
        $validator
            ->add('video_size', 'valid', ['rule' => 'numeric'])
            ->requirePresence('video_size', 'create')
            ->notEmpty('video_size');
            
        $validator
            ->requirePresence('video_file_type', 'create')
            ->notEmpty('video_file_type');
            
        $validator
            ->requirePresence('channel', 'create')
            ->notEmpty('channel');
            
        $validator
            ->requirePresence('video_trailer_url', 'create')
            ->notEmpty('video_trailer_url');
            
        $validator
            ->requirePresence('video_twitter', 'create')
            ->notEmpty('video_twitter');
            
        $validator
            ->requirePresence('video_fbpage', 'create')
            ->notEmpty('video_fbpage');
            
        $validator
            ->allowEmpty('video_referal');
            
        $validator
            ->add('total_like', 'valid', ['rule' => 'numeric'])
            ->requirePresence('total_like', 'create')
            ->notEmpty('total_like');
            
        $validator
            ->add('total_share', 'valid', ['rule' => 'numeric'])
            ->requirePresence('total_share', 'create')
            ->notEmpty('total_share');
            
        $validator
            ->add('total_viewed', 'valid', ['rule' => 'numeric'])
            ->requirePresence('total_viewed', 'create')
            ->notEmpty('total_viewed');
            
        $validator
            ->add('is_hd_sd', 'valid', ['rule' => 'boolean'])
            ->requirePresence('is_hd_sd', 'create')
            ->notEmpty('is_hd_sd');
            
        $validator
            ->add('is_paid', 'valid', ['rule' => 'boolean'])
            ->requirePresence('is_paid', 'create')
            ->notEmpty('is_paid');
            
        $validator
            ->add('price', 'valid', ['rule' => 'numeric'])
            ->requirePresence('price', 'create')
            ->notEmpty('price');
            
        $validator
            ->add('is_approved', 'valid', ['rule' => 'boolean'])
            ->requirePresence('is_approved', 'create')
            ->notEmpty('is_approved');
            
        $validator
            ->add('need_signup', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('need_signup');
            
        $validator
            ->add('modidfied', 'valid', ['rule' => 'date'])
            ->requirePresence('modidfied', 'create')
            ->notEmpty('modidfied');

        return $validator;
    }
    
    public function GetData() {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables
        */
         
        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
        */
        $aColumns = array( 'id', 'title', 'video_link','id');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "id";
         
        /* DB table to use */
        $sTable = "videos";
         
        //App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::get('default');
//        echo "<pre>";
//        print_r($dataSource->config());
         
        /* Database connection information */
        $gaSql['user']       = $dataSource->config()['username'];
        $gaSql['password']   = $dataSource->config()['password'];
        $gaSql['db']         = $dataSource->config()['database'];
        $gaSql['server']     = $dataSource->config()['host'];
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP server-side, there is
        * no need to edit below this line
        */
         
        /*
         * Local functions
        */
        function fatal_error ( $sErrorMessage = '' )
        {
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
         
         
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysql_pconnect( $gaSql['server'], $gaSql['user'], $gaSql['password']  ) )
        {
            fatal_error( 'Could not open connection to server' );
        }
         
        if ( ! mysql_select_db( $gaSql['db'], $gaSql['link'] ) )
        {
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
                    intval( $_GET['iDisplayLength'] );
        }
         
         
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= "`".$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."` ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
         
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }
         
         
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
         
        /* Individual column filtering */
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                $sWhere .= "`".$aColumns[$i]."` LIKE '%".mysql_real_escape_string($_GET['sSearch_'.$i])."%' ";
            }
        }
         
         
        /*
         * SQL queries
        * Get data to display
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".str_replace(" , ", " ", implode("`, `", $aColumns))."`
            FROM   $sTable
            $sWhere
            $sOrder
            $sLimit
            ";
        $rResult = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() );
        $aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(`".$sIndexColumn."`)
            FROM   $sTable
            ";
        $rResultTotal = mysql_query( $sQuery, $gaSql['link'] ) or fatal_error( 'MySQL Error: ' . mysql_errno() );
        $aResultTotal = mysql_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
         
        while ( $aRow = mysql_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                /*if ( $aColumns[$i] == "version" )
                {
                    $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
                }
                else if ( $aColumns[$i] != ' ' )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }*/
                
                if ( $i == 3 )
                {
                    //$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
                    $id=$aRow[$aColumns[$i]];
                    $columnvalue="<a href='".Router::url(['controller' => 'videos', 'action' => 'view', $id])."' style='text-decoration:none;'><span class='label label-default'>View</span></a>&nbsp;";
                    $columnvalue.="<a href='".Router::url(['controller' => 'videos', 'action' => 'edit', $id])."' style='text-decoration:none;'><span class='label label-info'>Edit</span></a>&nbsp;";
                    $row[] = $columnvalue;
                }
                else if ( $aColumns[$i] != ' ' )
                {
                    $row[] = $aRow[ $aColumns[$i] ];
                }
            }
            $output['aaData'][] = $row;
        }
         
        return $output;
    }
}
