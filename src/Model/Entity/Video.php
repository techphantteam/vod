<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Video Entity.
 */
class Video extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'userid' => true,
        'genres' => true,
        'topics' => true,
        'tags' => true,
        'countryid' => true,
        'video_title' => true,
        'video_year' => true,
        'video_description' => true,
        'video_director' => true,
        'video_synopsis' => true,
        'country' => true,
        'film_festival' => true,
        'language' => true,
        'video_logline' => true,
        'video_cast' => true,
        'video_link' => true,
        'video_length' => true,
        'video_size' => true,
        'video_file_type' => true,
        'channel' => true,
        'video_trailer_url' => true,
        'video_twitter' => true,
        'video_fbpage' => true,
        'video_referal' => true,
        'total_like' => true,
        'total_share' => true,
        'total_viewed' => true,
        'is_hd_sd' => true,
        'is_paid' => true,
        'price' => true,
        'is_approved' => true,
        'need_signup' => true,
        'modidfied' => true,
    ];
}
