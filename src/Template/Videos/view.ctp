<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Video'), ['action' => 'edit', $video->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Video'), ['action' => 'delete', $video->id], ['confirm' => __('Are you sure you want to delete # {0}?', $video->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Videos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Video'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="videos view large-10 medium-9 columns">
    <h2><?= h($video->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Video Title') ?></h6>
            <p><?= h($video->title) ?></p>
<!--            <h6 class="subheader"><?= __('Video Director') ?></h6>
            <p><?= h($video->video_director) ?></p>
            <h6 class="subheader"><?= __('Video Synopsis') ?></h6>
            <p><?= h($video->video_synopsis) ?></p>
            <h6 class="subheader"><?= __('Country') ?></h6>
            <p><?= h($video->country) ?></p>
            <h6 class="subheader"><?= __('Film Festival') ?></h6>
            <p><?= h($video->film_festival) ?></p>
            <h6 class="subheader"><?= __('Language') ?></h6>
            <p><?= h($video->language) ?></p>
            <h6 class="subheader"><?= __('Video Logline') ?></h6>
            <p><?= h($video->video_logline) ?></p>
            <h6 class="subheader"><?= __('Video Cast') ?></h6>
            <p><?= h($video->video_cast) ?></p>
            <h6 class="subheader"><?= __('Video Link') ?></h6>
            <p><?= h($video->video_link) ?></p>
            <h6 class="subheader"><?= __('Video File Type') ?></h6>
            <p><?= h($video->video_file_type) ?></p>
            <h6 class="subheader"><?= __('Channel') ?></h6>
            <p><?= h($video->channel) ?></p>
            <h6 class="subheader"><?= __('Video Trailer Url') ?></h6>
            <p><?= h($video->video_trailer_url) ?></p>
            <h6 class="subheader"><?= __('Video Twitter') ?></h6>
            <p><?= h($video->video_twitter) ?></p>
            <h6 class="subheader"><?= __('Video Fbpage') ?></h6>
            <p><?= h($video->video_fbpage) ?></p>-->
        </div>
        
      
    </div>
<!--    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Genres') ?></h6>
            <?= $this->Text->autoParagraph(h($video->genres)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Topics') ?></h6>
            <?= $this->Text->autoParagraph(h($video->topics)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Tags') ?></h6>
            <?= $this->Text->autoParagraph(h($video->tags)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Video Year') ?></h6>
            <?= $this->Text->autoParagraph(h($video->video_year)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Video Description') ?></h6>
            <?= $this->Text->autoParagraph(h($video->video_description)) ?>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Video Referal') ?></h6>
            <?= $this->Text->autoParagraph(h($video->video_referal)) ?>
        </div>
    </div>-->
    <div class="col-lg-12">
        <?php echo $this->Form->input('videos_id',['type'=>'hidden','value'=>$video->id]);?>
        <?php echo $this->Form->input('current_div',['type'=>'hidden','value'=>0]);?>
        <?php echo $this->Form->input('users_id',['type'=>'hidden','value'=>$user->id]);?>
        <?php echo $this->Form->input('parent_comment_id',['type'=>'hidden','value'=>0]);?>
        <?php echo $this->Form->input('name',['type'=>'hidden','value'=>$user->firstname.' '.$user->lastname]);?>
        <h6 class="subheader"><?= __('Comments') ?></h6>
        <div class="col-lg-1">
            <?php echo $this->Html->image('gravatar.jpg',['class'=>'img-class']);?>
        </div>
        <div class="col-lg-10">
            <?php echo $this->Form->input('user_comments', ['type' => 'textarea','class'=>'form-control text-resize','rows'=>'3']);?>
<!--            <textarea class="form-control text-resize" rows="3"></textarea>-->
            <br>
            
            <button type="submit" class="btn btn-primary post-btn" onclick="writecomment('<?php echo $this->Url->build('/', true)?>');">Post</button>
        </div>
    </div>
    <div class="col-lg-12">
    
    </div>
    <div class="col-lg-12" id="usercurrent_comment">
        <?php foreach ($data->response->data as $value){
            echo '<div class="whole-class" id="userreply'.$value->id.'"><span>'.$value->name.':<span class="time-stamp">'.$this->Elapse->humanTiming(strtotime($value->updated_date)).' ago</span>';
            if($value->users_id!=$user->id)
            {
                echo "<span class='reply-class'><a onclick='setReply($value->users_id,\"{$value->name}\",\"userreply{$value->id}\")'>reply</a></span>";
            }
            echo '</span><p>'.$value->comments.'</p>';
            echo '</div>';
            
        }
?>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title" id="myLargeModalLabel">Comment</h4>
        </div>
        <div class="modal-body">
           <?php echo $this->Form->input('reply_comments', ['type' => 'textarea','class'=>'form-control text-resize','rows'=>'3']);?>
           <button type="submit" class="btn btn-primary post-btn" onclick="writecomment('<?php echo $this->Url->build('/', true)?>');">Post</button>
               
           <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
  </div>
</div>
