<!--<form id="uploadForm" action="ajaxUpload" method="post">
    <div>
        <label>Upload Image File:</label>
        <input name="userImage" id="userImage" type="file" class="demoInputBox" />
    </div>
    <div><input type="submit" id="btnSubmit" value="Upload" class="btnSubmit" /></div>
    <div id="progress-div"><div id="progress-bar"></div></div>
    <div id="targetLayer"></div>
</form>-
<div id="loader-icon" style="display:none;"><?= $this->Html->image('loader.gif', array('alt' => 'Loader', 'border' => '0')); ?></div>-->
<div id="step1">
    <!--    <form id="uploadForm" action="ajaxUpload" method="post">-->
    <form id="uploadForm" action="uploadAjax" method="post">
        <?php
        echo "<legend>Step1:Upload Video</legend>";
        echo $this->Form->file('file', ['id' => 'video']);
        echo $this->Form->button('Upload', ['id' => 'btnupload']);
        echo '<div id="loader-icon" style="display:none;">';
        echo $this->Html->image('loader.gif', array('alt' => 'Loader', 'border' => '0'));
        echo '</div>';
        ?>
    </form>
    <div class="progress">
        <div class="progress-bar" id="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            0%
        </div>
    </div>
</div>

<div class="videos form large-10 medium-9 columns" id='step2' style='display:none'>
    <?= $this->Form->create(null, ['url' => ['action' => 'edit']]) ?>
    <fieldset>
        <legend><?= __('Step2:Add Video') ?></legend>
        <?php
            echo $this->Form->hidden('id',['id'=>'id']);
            echo $this->Form->input('genres');
            echo $this->Form->input('topics');
            echo $this->Form->input('tags');
            echo $this->Form->input('countryid');
            echo $this->Form->input('video_title');
            echo $this->Form->input('video_year');
            echo $this->Form->input('video_description');
            echo $this->Form->input('video_director');
            echo $this->Form->input('video_synopsis');
            echo $this->Form->input('country');
            echo $this->Form->input('film_festival');
            echo $this->Form->input('language');
            echo $this->Form->input('video_logline');
            echo $this->Form->input('video_cast');
            echo $this->Form->input('video_link',['id'=>'video_link']);
            echo $this->Form->input('video_length');
            echo $this->Form->input('video_size');
            echo $this->Form->input('video_file_type');
            echo $this->Form->input('channel');
            echo $this->Form->input('video_trailer_url');
            echo $this->Form->input('video_twitter');
            echo $this->Form->input('video_fbpage');
            echo $this->Form->input('video_referal');
            echo $this->Form->input('total_like');
            echo $this->Form->input('total_share');
            echo $this->Form->input('total_viewed');
            echo $this->Form->input('is_hd_sd');
            echo $this->Form->input('is_paid');
            echo $this->Form->input('price');
            echo $this->Form->input('is_approved');
            echo $this->Form->input('need_signup');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>


<script>
    $(document).ready(function () {
        //alert("ok");
        $('#uploadForm').submit(function (e) {
            e.preventDefault();
            if ($('#video').val()) {
                $('#loader-icon').show();
                $(this).ajaxSubmit({
                    beforeSubmit: function () {
                        $("#progress-bar").width('0%');
                    },
                    uploadProgress: function (event, position, total, percentComplete) {
                        $("#progress-bar").width(percentComplete + '%');
                        $("#progress-bar").text(percentComplete + ' %');
                    },
                    success: function (data) {
                        $('#loader-icon').hide();
                        var json = JSON.parse(data);
                        if (json.response.code == "200") {
                            alert('Video Uploaded Successfully Please Fill Other Information');
                            $('#loader-icon').hide();
                            $('#step1').hide();
                            $('#step2').show();
                            $('#video_link').val(json.response.url);
                            $('#id').val(json.response.id);
                        } else {
                            alert('Video Upload Failed');
                        }
                    },
                    resetForm: true
                });

                return false;
            } else {
                alert("Please Select Video");
            }
        });
    });
</script>