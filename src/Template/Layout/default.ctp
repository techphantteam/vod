<!DOCTYPE html>
<html lang="en">
    <?= $this->element('htmlhead'); ?>
    <body>
        <div id="wrapper">  <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <?= $this->element('header'); ?>
                <?php if ($this->request->Session()->read('userdata')->role == 'Admin'): ?>
                    <?= $this->element('adminsidebar'); ?>
                <?php elseif ($this->request->Session()->read('userdata')->role == 'Creator'): ?>
                    <?= $this->element('creatorsidebar'); ?>
                <?php elseif ($this->request->Session()->read('userdata')->role == 'Super Admin'): ?>
                    <?= $this->element('superadminsidebar'); ?>
                <?php endif; ?>
            </nav>
            <div id="page-wrapper">

                <div class="container-fluid" style="min-height: 485px;">
                    <?= $this->fetch('content') ?>
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <script type="text/javascript">
            $(document).ready(function () {
                $('#vimeoapitable').dataTable();
            });
        </script>
         <?= $this->Html->script('adminjs/comment') ?>
    </body>
</html>