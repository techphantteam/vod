<?php   echo $this->Flash->render('usersignup'); ?>   

<?php
echo "<legend>CrumbList</legend>";
$this->Html->addCrumb('Home', '/');
$this->Html->addCrumb('Pages', ['controller' => 'pages']);
$this->Html->addCrumb('About', ['controller' => 'pages', 'action' => 'about']);
echo $this->Html->getCrumbList();

echo "<legend>Icon</legend>";
echo $this->Html->icon('pencil');

echo "<legend>Messages</legend>";
echo $this->Html->alert('This is a warning alert!');

echo $this->Html->alert('This is a success alert!', 'success');

echo $this->Html->alert('This is a info alert with a specific id!', [
    'id' => 'alert-info',
    'type' => 'info'
]);


echo "<legend>Form Vertical</legend>";
echo $this->Form->create();
echo $this->Form->input('username', ['type' => 'text']);
echo $this->Form->input('password', ['type' => 'password']);
echo $this->Form->input('remember', ['type' => 'checkbox']);
echo $this->Form->submit('Log In');
echo $this->Form->end();


echo "<legend>Form Horiz.</legend>";
echo $this->Form->create(null, ['horizontal' => true]);
echo $this->Form->input('username', ['type' => 'text']);
echo $this->Form->input('password', ['type' => 'password']);
echo $this->Form->input('remember', ['type' => 'checkbox']);
echo $this->Form->submit('Log In');
echo $this->Form->end();

echo $this->Form->create(null, [
    'horizontal' => true,
    'cols' => [ // Total is 12, default is 2 / 6 / 4
        'label' => 2,
        'input' => 6,
        'error' => 4
    ]
]);


echo $this->Form->create(null, [
    'horizontal' => true,
    'cols' => [
        'sm' => [
            'label' => 4,
            'input' => 4,
            'error' => 4
        ],
        'md' => [
            'label' => 2,
            'input' => 6,
            'error' => 4
        ]
    ]
]);


echo $this->Form->input('username', ['type' => 'text']);
echo $this->Form->input('password', ['type' => 'password']);
echo $this->Form->input('remember', ['type' => 'checkbox']);
echo $this->Form->submit('Log In');
echo $this->Form->end();


echo $this->Form->file('file');


echo $this->Form->buttonGroup([$this->Form->button('1'), $this->Form->button('2')]);


echo $this->Form->buttonToolbar([
    $this->Form->buttonGroup([$this->Form->button('1'), $this->Form->button('2')]),
    $this->Form->buttonGroup([$this->Form->button('3'), $this->Form->button('4')])
]);

echo $this->Form->dropdownButton('My Dropdown', [
    $this->Html->link('Link 1', '#'),
    $this->Html->link('Link 2', '#'),
    'divider',
    $this->Html->link('Link 3', '#')
]);


echo $this->Form->input('mail', [
        'prepend' => '@', 
        'help' => 'Hey guy, you need some help?',
        'append' => $this->Form->button('Send')
    ]) ;


 echo $this->Form->input('mail', [
        'append' => [
            $this->Form->button('Button'),
            $this->Form->dropdownButton('Dropdown', [
                $this->Html->link('A', '#'), 
                $this->Html->link('B', '#'),
                'divider', 
                $this->Html->link('C', '#')
            ])
        ]
    ]) ;
?>

