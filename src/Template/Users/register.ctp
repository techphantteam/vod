
<?php

$firstname = "";
$lastname = "";
$email = "";
$emailvalid="";
$username = "";
$pass = "";
$cpass = "";
$plength="";
$cplength="";
$passconfirm="";
$userunique="";
$regemail="";
if (isset($response_api)) {
    if (isset($response_api->errormessage->firstname->_empty)) {
        $firstname = $response_api->errormessage->firstname->_empty;
    }if (isset($response_api->errormessage->lastname->_empty)) {
        $lastname = $response_api->errormessage->lastname->_empty;
    }if (isset($response_api->errormessage->email->_empty)) {
        $email = $response_api->errormessage->email->_empty;
    }if (isset($response_api->errormessage->username->_empty)) {
        $username = $response_api->errormessage->username->_empty;
    }if (isset($response_api->errormessage->password->_empty)) {
        $pass = $response_api->errormessage->password->_empty;
    }if (isset($response_api->errormessage->cpassword->_empty)) {
        $cpass = $response_api->errormessage->cpassword->_empty;
    }if(isset($response_api->errormessage->email->valid)){
        $emailvalid=$response_api->errormessage->email->valid;
    }if(isset($response_api->errormessage->password->length)){
        $plength=$response_api->errormessage->password->length;
    }if(isset($response_api->errormessage->cpassword->length)){
        $cplength=$response_api->errormessage->cpassword->length;
    }if(isset($response_api->errormessage->cpassword->passwordsEqual)){
        $passconfirm=$response_api->errormessage->cpassword->passwordsEqual;
    }if(isset($response_api->errormessage->username->unique)){
        $userunique=$response_api->errormessage->username->unique;
    }if(isset($response_api->errormessage->email->unique)){
        $regemail=$response_api->errormessage->email->unique;
    }
    
    
}
echo '<div clas>';
echo '<div class="col-md-5">';
echo '</div>';
echo '<div class="col-md-9">';
echo $this->Flash->render();
echo '<div class="back">';
echo '<label><h1><i class="fa fa-sign-in"></i> Sign Up</h1></label>';
echo $this->Form->create(null, ['horizontal' => true]);
echo $this->Form->hidden('role', ['value' => 'Creator']);
echo $this->Form->input('firstname', ['placeholder' => 'Enter Firstname', 'label' => 'First Name']);
echo $firstname;
echo $this->Form->input('lastname', ['placeholder' => 'Enter Lastname', 'label' => 'Last Name']);
echo $lastname;
echo $this->Form->input('email', ['placeholder' => 'Enter Your Email', 'label' => 'Email']);
echo $email;echo $emailvalid;echo $regemail;
echo $this->Form->input('username', ['type' => 'text', 'placeholder' => 'Enter User Name']);
echo $username;echo $userunique;
echo $this->Form->input('password', ['type' => 'password', 'placeholder' => 'Enter Password']);
echo $pass;echo $plength;
echo $this->Form->input('cpassword', ['type' => 'password', 'placeholder' => 'Enter Password']);
echo $cpass;echo $cplength; echo'<br>';echo $passconfirm;
echo $this->Form->submit('Register', ['class' => 'btn btn-primary btn-block']);
echo '<div class="form-group>';
echo '<span class="col-md-3"><label>Member</label> ';
echo $this->Html->link('Sign In ?', ['controller' => 'Users', 'action' => 'login', '_full' => true], ['class' => 'anchor1']);
echo '</span>';
echo '</div>';
echo $this->Form->end();
echo '</div>';
echo '</div>';
echo '</div>';
?>